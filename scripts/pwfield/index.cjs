module.exports = callback => {
	const crypto = require('node:crypto');
	const base64URLEncode = str =>
		str.toString('base64')
			.replace(/\+/g, '-')
			.replace(/\//g, '_')
			.replace(/=/g, '');

	/** @param {buffer} buffer - fs.readStream data input */
	const sha256 = buffer =>
		crypto.createHash('sha256')
			.update(buffer)
			.digest();

	const VERIFIER = base64URLEncode(crypto.randomBytes(32));
	const CHALLENGE = base64URLEncode(sha256(VERIFIER));
	const noderesult = {
		VERIFIER,
		CHALLENGE,
	};

	/* eslint no-warning-comments: "error" */
	const rainbowHashInputTest = async _params => {
		// Make the function to input usernames & passwords into fields after
		// testing basic hash functions and such
	};

	/** Crack the password on the OSMTH Login page.
   * @param {string[]} params - Different combinations to try. */
	(async params => {
		// Select the input fields we want.
		const username = document.getElementById('modlgn-username');
		const password = document.getElementById('modlgn-password');

		username.value = params.username;
		password.value = params.password;

		callback(null, noderesult);
	}).catch(err => {
		console.error(err.stack);
	}).then(() => {
		console.log('uwu~');
	}).finally(() => {
	})(rainbowHashInputTest([202040240232031, 12309123012, 1024023942, 2349203490]))();
};
