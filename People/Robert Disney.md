# Robert Disney

[[deceasesd]] since ~2015-16

> [AllPeople profile](https://allpeople.com/robert+disney_osmth-us)

> [AllBiz Profile](https://www.allbiz.com/business/osmth-941-698-4010)

> [Email](<grandcommander@osmth.org>)

> [Open Corporate](https://opencorporates.com/companies/us_fl/P01000041590)

> [Open Corporate (archive link)](https://archive.is/i9qEx)

## Significance

Owned [[Sigma International Holdings]],
specialising in utilising detectives, bodyguards and employing the use of armored cars.

Later on became the CTO of his wife's company, [[Cass-Sigma JV]]

## Relations

Wife is [[Sheila Disney]], who went on to found [[Cass-Sigma JV]]

## Known Locations

**Home Address**

```log
548 Boundary Blvd, 33947
(941) 698-4010
```

![map](548boundaryblvd.png)

**Business Address**

```log
  2800 PLACIDA RD STE 110
  ENGLEWOOD, FL
  342245500 
```

Former OSMTH Grand Commander, not to be confused with the webmaster (identity unknown).

[//begin]: # "Autogenerated link references for markdown compatibility"
[Sigma International Holdings]: <../Companies/Sigma International Holdings.md> "Sigma International Holdings"
[Cass-Sigma JV]: <../Companies/Cass-Sigma JV.md> "Cass-Sigma JV"
[Sheila Disney]: <Sheila Disney.md> "Sheila Disney"
[//end]: # "Autogenerated link references"